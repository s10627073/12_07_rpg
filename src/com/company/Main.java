package com.company;
import game.MichaelMyers;
import game.Nurse;
import game.Roles;
import  game.Doctor;
import game.Freddy;
import  game.Clown;
import  game.LeatherFace;

//匯入角色類別
public class Main {
    public static void main(String[] args) {
	// write your code here
        Nurse nurse = new Nurse("護士");
      //  doNurse(nurse);
        doWork(nurse);
        MichaelMyers michaelMyers = new MichaelMyers("麥克邁爾斯");
        doWork(michaelMyers);
        Doctor doctor = new Doctor("醫生");
        doWork(doctor);
        Freddy freddy = new Freddy("佛萊迪");
        doWork(freddy);
        Clown clown = new Clown("小丑");
        doWork(clown);
        LeatherFace leatherFace =  new LeatherFace("人皮臉");
        doWork(leatherFace);

    }
    public static void doWork(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doskill();
    }
    //public static void doNurse(Nurse nurse){
     //   System.out.println(nurse);
      //  nurse.doAttack();
       // nurse.doskill();
    //}
}
