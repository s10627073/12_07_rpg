package game;

public class Doctor extends Roles {
    public Doctor(String name){
        super(name);
    }
    //繼承Roles
    @Override
    public void doAttack() {
        System.out.println("我是" + getName() + "我用狼牙棒攻擊人");
    }

    @Override
    public void doskill() {
        System.out.println("使用技能為電擊");
    }
}
