package game;
import java.util.List;
public abstract class Roles implements IActions {
    private String name;
    private int level;
    private int attack;
    private int skill;
    private  int weapon;
    //每個共同屬性
    public Roles(String name,int level){
        this.name = name;
        this.level = level;
        //建構子
    }

    public Roles(String name){
        this(name,1);
    //初始設定值
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }
    public int getWeapon() {
        return weapon;
    }

    public void setWeapon(int weapon) {
        this.weapon = weapon;
    }
    @Override
    public String toString(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("我叫:").append(getName()).append("我的等級:").append(getLevel());
        return stringBuffer.toString();
        //輸入每個初始化的名子與等級
    }
}
